<?php

namespace Alpipego\TIL;

use Alpipego\WpLib\Autoload;
use Alpipego\WpLib\Custom\Capabilities;
use Alpipego\WpLib\Custom\PostType;
use Alpipego\WpLib\DIContainer;

/**
 * Plugin Name: TIL for WordPress
 * Plugin URI: https://gitlab.com/alpipego/wp-til
 * Description: Add a CPT for TIL
 * Author: alpipego
 * Version: 1.0.2
 * Author URI: http://alpipego.com/
 * License: MIT
 */

new Autoload( __DIR__ . '/src', __NAMESPACE__ );

$c = new DIContainer();

$c['til_cpt'] = function () {
	return ( new PostType( 'til', 'TIL', 'TIL' ) )
		->menu_icon( 'dashicons-welcome-learn-more' )
		->capObj( new Capabilities() )
		->public( true )
		->capability_type( 'til' )
		->has_archive( false );
};

add_action( 'plugins_loaded', function () use ( $c ) {
	$c->run();
} );

# TIL

Adds a custom post type `til`.

## Installation
Has to be installed via composer. Add this to your WordPress `composer.json`

```
    {
        "type": "vcs",
        "url": "git@gitlab.com:alpipego/wp-til.git"
    },
    "require": {
        "alpipego/til": "~1.0"
    }
```
